from guardian import GuardStateDecorator
import numpy as np
from cdsutils import avg, getdata
import gpstime
import cdslib
from static_tester import StaticTester
from timeout_utils import timeout
from SYS_DIAG import *

SYSDIAG.log = False

"""
As of June 29, 2022  these tests are being temporarily removed 
with the intention of putting them back later as they are needed:

HW_INJ
SR3_HEATER
SQZ OPO REFL

Tests to improve:
ESD
OMC_PZT_HV
Coil driver health

"""
##################################################

suspensions = {
    'BS': ['M1', 'M2'],
    'ITMX': ['M0', 'R0', 'L1', 'L2'],
    'ITMY': ['M0', 'R0', 'L1', 'L2'],
    'ETMX': ['M0', 'R0', 'L1', 'L2'],
    'ETMY': ['M0', 'R0', 'L1', 'L2'],
    'PRM': ['M1', 'M2', 'M3'],
    'PR2': ['M1', 'M2', 'M3'],
    'PR3': ['M1', 'M2', 'M3'],
    'MC1': ['M1', 'M2', 'M3'],
    'MC2': ['M1', 'M2', 'M3'],
    'MC3': ['M1', 'M2', 'M3'],
    'SRM': ['M1', 'M2', 'M3'],
    'SR2': ['M1', 'M2', 'M3'],
    'SR3': ['M1', 'M2', 'M3'],
    'FC1': ['M1', 'M2', 'M3'],
    'FC2': ['M1', 'M2', 'M3'],
    'OMC': ['M1'],
    'TMSX': ['M1'],
    'TMSY': ['M1'],
    'RM1': ['M1'],
    'RM2': ['M1'],
    'IM1': ['M1'],
    'IM2': ['M1'],
    'IM3': ['M1'],
    'IM4': ['M1'],
    'OM1': ['M1'],
    'OM2': ['M1'],
    'OM3': ['M1'],
    'OPO': ['M1'],
    'OFI': ['M1'],
    # While the ZMs have an M2 stage, they dont have an M2 WD
    'ZM1': ['M1'],
    'ZM2': ['M1'],
    'ZM3': ['M1'],
    'ZM4': ['M1'],
    'ZM5': ['M1'],
    'ZM6': ['M1'],
}

##################################################
# Useful functions/classes

@timeout()
def get_avg(*args):
    return avg(*args)

# --->>> Use this to get averages <<<----
def try_avg(*args):
    counter = 0
    # Only try three times, if it still cant get the data then sometime must be wrong
    while counter < 4:
        av = get_avg(*args)
        if av:
            return av
        else:
            log('Failed to get data, arguments: {}'.format(args))
            counter += 1
    # We've failed a few times, giving up and returning None
    log('Giving up trying to get data')
    return None

def RMS(current_channel):
    """Takes the RMS by taking the square root of the average^2 + stdev^2"""
    avg_stddev = avg(-5, current_channel, 2)
    rms = np.sqrt(np.square(avg_stddev[0]) + np.square(avg_stddev[1]))
    return rms


def percent_diff(first, second):
    """Finds and returns the percent difference betweent the two values given"""
    local_avg = float(first + second) / 2
    try:
        diff = abs(first - second) / local_avg
    except ZeroDivisionError:
        # Good enough for government work
        diff = abs(first - second) / 0.0000000001
    percent = diff * 100
    return percent


def PSL_on():
    if ezca['PSL-LASER_AMP2_PWR'] < 130:
        return False
    else:
        return True

##################################################
# TESTS

als_timer = {'polarization': 0}

@SYSDIAG.register_test
def ALS():
    """ALS checks

    This can be seen when the beatnote drops down to 0-2 MHz and when
    (in dBm) it gets above 5.

    Checks to see if the ALS SHG Temperature Control Servo is on
    Nominal is 'ON'

    """
    dBmBeatnotelimit = 5
    MHzBeatnotelimit = 2
    ends = ['X', 'Y']
    diffcomm = ['DIFF', 'COMM']
    pol_controller_max_time = 600

    # Check the laser is lasing. We can double check with 2 diodes each
    lasing_thresh = 0.2
    for end in ends:
        if ezca['ALS-{}_LASER_HEAD_LASERDIODE1POWERMONITOR'.format(end)] < lasing_thresh \
           and ezca['ALS-{}_LASER_HEAD_LASERDIODE2POWERMONITOR'.format(end)] < lasing_thresh:
            yield 'ALS {} not lasing'.format(end)

    # check the beatnotes to see if their signals are changed from oscillations in the laser
    for end in ends:
        if (ezca['ALS-%s_FIBR_A_DEMOD_RFMON' % (end)] > dBmBeatnotelimit)\
           and (ezca['ALS-%s_FIBR_LOCK_BEAT_FREQUENCY' % (end)] < MHzBeatnotelimit):
            yield "%s laser oscillating" % (end)
    # Make sure that the SHG servo is ON
    #if ezca['ALS-C_SHG_TEC_SERVO'] == 0:
    #    yield "SHG temp control servo OFF"
    # Make sure there is no force on PLL Slow
    if ezca['ALS-C_COMM_PLLSLOW_FORCE'] != 0:
        yield 'COMM_PLLSLOW_FORCE is forced'

    # ALS PLL COMM/DIFF Presets
    # 1 == Low
    if ezca['GRD-ISC_LOCK_STATE_N'] > 451:
        for i in diffcomm:
            if ezca['ALS-C_{}_PLL_PRESET'.format(i)] != 1:
                yield 'PLL {} Preset is not nominal (1/Low)'.format(i)

    # Check fiber polarization, this is also done by the ALS grds but they
    # look at the beckhoff chans, we dont want to only do that
    # JCD 25Apr2019: only do this check if we're acquiring, not when in NomLowNoise 
    if ezca['GRD-ISC_LOCK_STATE_N'] < 307:
        #for end in ends:
        #    if ezca['ALS-{}_FIBR_LOCK_FIBER_POLARIZATIONPERCENT'.format(end)] > 20:
        #        yield 'ALS {} fiber polarization is > 20%'.format(end)
        if ezca['ALS-X_FIBR_LOCK_FIBER_POLARIZATIONPERCENT'] > 30:
            yield 'ALS X fiber polarization is > 30%'
        if ezca['ALS-Y_FIBR_LOCK_FIBER_POLARIZATIONPERCENT'] > 20:
            yield 'ALS Y fiber polarization is > 20%'

    # Make sure that the fiber polarization controller is not left on
    # 0=undefined, 1=ac power on, 2=ac off
    if ezca['CDS-PULIZZI_ACPWRCTRL_MSR0_OUTLET_1_STATUS'] == 1:
        time_now = int(gpstime.tconvert())
        if als_timer['polarization'] == 0:
            als_timer['polarization'] = time_now
        elif time_now - als_timer['polarization'] > pol_controller_max_time:
            yield 'Polarization controller ON'
    # Reset the timer
    elif als_timer['polarization'] != 0:
        als_timer['polarization'] = 0


@SYSDIAG.register_test
def ASC_OUTPUT():
    """Check that the ASC signals have not accidentally been turned OFF.

    """
    if ezca['ASC-WFS_GAIN'] != 1:
        yield 'ASC WFS not ON'
    ### Add indiviual optic filters (ASC-PRM_YAW) output
    asc_sus = ['PRM', 'PR2', 'PR3', 'BS', 'ITMX', 'ITMY', 'ETMX', 'ETMY', 'SRM', 'SR2', 'SR3',
               'IM1', 'IM2', 'IM3', 'IM4', 'RM1', 'RM2', 'OM1', 'OM2', 'TMSX', 'TMSY']
    for sus in asc_sus:
        for dof in ['PIT', 'YAW']:
            # Check the gain is 1
            if ezca['ASC-{}_{}_GAIN'.format(sus, dof)] != 1:
                yield '{} {} gain is not 1'.format(sus, dof)
            # Check the Input and Output are ON
            lf = ezca.LIGOFilter('ASC-{}_{}'.format(sus, dof))
            if not lf.is_input_on() or not lf.is_output_on():
                yield 'ASC {} {} input or output is not ON'.format(sus, dof)


@SYSDIAG.register_test
def BEAM_DIVERTERS():
    """The Beam divereters should be closed after the ISC_LOCK state of
    CLOSE_BEAM_DIVERTERS #550. 1 == closed, 0 == open.
    """
    #divs1 = {'REFL':'A', 'AS':'D', 'POP':'B'} # POP must be open for POP_X use.  Hopefully will be able to close in the future.  1Oct2016 JCD
    divs1 = {'REFL': 'A', 'AS': 'D'}
    divs2 = {'C': 'C', 'X': 'A', 'Y': 'A'}
    if ezca['GRD-ISC_LOCK_STATE_N'] > 590:
        for div in divs1:
            if ezca['SYS-MOTION_C_BDIV_{}_POSITION'.format(divs1[div])] != 1:
                yield '{} beam diverter is not closed'.format(div)

    for div in divs2:
        if ezca['SYS-MOTION_{}_BDIV_{}_POSITION'.format(div, divs2[div])] != 1:
            if div == 'C':
                #if ezca['GRD-SQZ_MANAGER_STATE'] != 'SQUEEZING':
                #    yield 'OMC beam diverter is not closed'
                #else:
                #    continue
                continue
            else:
                yield '{} beam diverter is not closed'.format(div)


beck_gen_chan = 'SYS-ETHERCAT_{}_ELAPSEDTIME'
beck_loc = ['AUXCORNER', 'ISCCORNER', 'TCSCORNER', 'SQZCORNER', 'AUXENDX', 'ISCENDX', 'TCSENDX', 'AUXENDY', 'ISCENDY', 'TCSENDY']
beck_chans = [beck_gen_chan.format(loc) for loc in beck_loc]
beck_st = StaticTester(beck_chans)

@SYSDIAG.register_test
def BECKHOFF():
    msg = 'Beckhoff {}1_PLC1 crashed'
    for loc in beck_loc:
        if beck_st.is_chan_static(beck_gen_chan.format(loc)):
            yield msg.format(loc)


brs_c_chan_gen = 'ISI-GND_BRS_ETM{}_CBIT'
brs_st = StaticTester([brs_c_chan_gen.format(end) for end in ['X', 'Y']],
                      val=0,
                      static_time=60)
@SYSDIAG.register_test
def BRS_CHECK():
    """Check the two end station Beam Roation Sensors to make sure that
    they are not in FAULT (as read from their Guardian status node state.
    Also checks the status of the CBIT, to see if C code is live
    """
    for end in ['X', 'Y']:
        if brs_st.is_chan_static(brs_c_chan_gen.format(end)):
            yield 'BRS {} C code has stopped'.format(end)
        if ezca['GRD-BRS{}_STAT_STATE'.format(end)] == 'FAULT':
            yield 'BRS {} is in FAULT'.format(end)
        if ezca['ISI-GND_BRS_ETM{}_R{}_BLRMS_300M_1'.format(end, 'X' if end == 'Y' else 'Y')] > 20:
            yield 'BRS{} is noisy'.format(end)


@SYSDIAG.register_test
def CDS_CA_COPY():
    """Watch that the CDS_CA_COPY node is still good. It may be a bit
    hidden, so this should serve as a more visible notification if
    something has gone wrong.
    """
    if ezca['GRD-CDS_CA_COPY_ERROR'] != 0:
        yield 'Node has an error, copying may not work.'


camera_chans = []
for xy in ['X', 'Y']:
    for i in ['YAW_POS', 'PIT_POS', 'SUM']:
        camera_chans.append('ALS-{}_CAM_ITM_{}'.format(xy, i))
cam_st = StaticTester(camera_chans)

@SYSDIAG.register_test
def CDS_CAMERA_COPY():
    """The script (userapps)/sys/h1/scripts/h1cameracopy.py will copy over the
    digital image as an EPICS record. Make sure that the values are updating.
    """
    for chan in camera_chans:
        if cam_st.is_chan_static(chan):
            yield '{} not updating'.format(chan)
            break


# Taking this test out until its future is determined (5-5-16)
#@SYSDIAG.register_test
def COIL_DRIVERS():
    """Coil drivers functioning
    If they stay at 0, it will bring up a notification
    """
    upquad = ['LF', 'RT', 'SD', 'F1', 'F2', 'F3']
    upper = ['T1', 'T2', 'T3', 'SD', 'LF', 'RT']
    lower = ['UL', 'UR', 'LL', 'LR']

    quad = {'M0': upquad, 'R0': upquad, 'L1': lower, 'L2': lower}
    bs = {'M1': upquad, 'M2': lower}
    triple = {'M1': upper, 'M2': lower, 'M3': lower}
    single = {'M1': upper}
    suspensions = {
        'BS': bs,
        'ITMX': quad,
        'ITMY': quad,
        'ETMX': quad,
        'ETMY': quad,
        'PRM': triple,
        'PR2': triple,
        'PR3': triple,
        'MC1': triple,
        'MC2': triple,
        'MC3': triple,
        'SRM': triple,
        'SR2': triple,
        'SR3': triple,
        'OMC': single,
        'TMSX': {'M1': upquad},
        'TMSY': {'M1': upquad}
    }

    for sus, level in suspensions.items():
        for lev, osem in level.items():
            for ose in osem:
                if sus in ['BS', 'ITMX', 'ITMY', 'ETMX', 'ETMY', 'TMSX', 'TMSY']:
                    if ezca['SUS-%s_%s_NOISEMON_%s_OUTMON' % (sus, lev, ose)] == 0:
                        avgstd = try_avg(-2, 'SUS-%s_%s_NOISEMON_%s_OUTMON' % (sus, lev, ose))
                        if avgstd == 0:
                            yield '%s_%s_%s noisemon is dead' % (sus, lev, ose)
                else:
                    if ezca['SUS-%s_%s_NOISEMON_%s_MON' % (sus, lev, ose)] == 0:
                        avgstd = try_avg(-2, 'SUS-%s_%s_NOISEMON_%s_MON' % (sus, lev, ose))
                        if avgstd == 0:
                            yield '%s_%s_%s noisemon is dead' % (sus, lev, ose)


def get_iop_models():
    """Use this function rather than some list to stay up
    to date with the models.
    """
    models = list(cdslib.get_all_models())
    for model in models:
        if 'iop' in model.name:
            yield model

# Dont forget to make this a list so we can keep iterating
iop_models = list(get_iop_models())
# The dict will have all the values at 0 if all is well
DAC_ERROR_DICT = {model.name: 0 for model in iop_models}


@SYSDIAG.register_test
def DAQ_ERROR():
    """Look for the state word bit to go to 1 for > 5min

    """
    wait_time = 300
    for model in iop_models:
        # DAC error when the fourth bit is one
        # FIXME: This test didnt seem to wok on Jun 25 2018. Review, and rethink
        # FIXME: removing MY for the moment.
        if model.DCUID == 122:
            continue
        if (int(ezca['FEC-{}_STATE_WORD'.format(model.DCUID)]) & 2 ** 4) == 2 ** 4:
            #log('Test text:  {}'.format(model.name))
            # Check if this is the first offense
            if DAC_ERROR_DICT[model.name] == 0:
                # Mark the start of the error
                DAC_ERROR_DICT[model.name] = int(gpstime.gpsnow())
            # If it isnt the first time, then see how long ago
            elif int(gpstime.gpsnow()) - DAC_ERROR_DICT[model.name] > wait_time:
                yield '{}\'s DAC has been killed'.format(model.name)
        # All is good, set value to 0
        else:
            DAC_ERROR_DICT[model.name] = 0


@SYSDIAG.register_test
def ESD_DRIVER():
    """ESD driver status

    """
    for optic in ['ITMX', 'ITMY', 'ETMX', 'ETMY']:
        if abs(ezca['SUS-{}_L3_ESDAMON_DC_INMON'.format(optic)]) < 10:
            if optic == ('ETMX' or 'ETMY'):
                yield 'ESD DC HV {} appears OFF'.format(optic)
            elif ezca['SUS-{}_L3_ESDOUTF_DC_OUTPUT'.format(optic)] != 0:
                yield 'ESD DC HV {} appears OFF'.format(optic)

    # EX
    dc_nom = -32300
    dc_nom_dev = abs(dc_nom * 0.1)
    quadr_thresh = 8000
    if ezca['GRD-ISC_LOCK_STATE_N'] < 500:
        dcmon = ezca['SUS-ETMX_L3_ESDAMON_DC_INMON']
        if ((dc_nom - dc_nom_dev) >= dcmon \
            or (dc_nom + dc_nom_dev) < dcmon \
            and ezca['SUS-ETMX_L3_ESDAMON_UL_INMON'] < quadr_thresh):
            yield 'ESD X driver OFF'


@SYSDIAG.register_test
def EXT_ALERTS():
    """Check that the scripts updating the site channels
    with external alert notifications are running. There are
    query time channels that wont update if the scripts have stopped.
    Two to check: ext_alert and standdown_alert

    #TODO: Add wiki links to these if they go down!
    """
    cur_gps = ezca['FEC-42_TIME_DIAG']
    if cur_gps - ezca['CAL-INJ_EXTTRIG_ALERT_QUERY_TIME'] > 120:
        yield 'GraceDB query failure'
    # 20240821 TJS - Removed temporarily until excess Fermi GRBs fixed
    #if cur_gps - ezca['OPS-STANDDOWN_EVENT_QUERY_GPS'] > 240:
    #    yield 'Stand down query failure'


@SYSDIAG.register_test
def EXTTRIG():
    """Display if there is a SNEWS alert"""
    if ezca['CAL-INJ_EXTTRIG_ALERT_SOURCE'] == 'SNEWS':
        if gpstime.gpsnow() - ezca['CAL-INJ_EXTTRIG_ALERT_TIME'] < 3600:
            yield 'SNEWS alert active'


@SYSDIAG.register_test
def FAST_SHUTTER_HV():
    """Make sure the Fast Shutter high voltage is READY

    """
    # 1 is 'Ready', the str value didnt seem to work
    if ezca['SYS-MOTION_C_FASTSHUTTER_A_READY'] != 1:
        yield 'Fast Shutter HV not Ready'


@SYSDIAG.register_test
def HEPI_PUMP_PRESSURE():
    """Check hepi pump pressures are nominal (69-71 psi)

    """
    pressures = {'CS': 'HPI-PUMP_L0_DIFF_PRESSURE',
                 'EY': 'HPI-PUMP_EY_PRESS_DIFF_PSI',
                 'EX': 'HPI-PUMP_EX_PRESS_DIFF_PSI'}

    for loc, channel in pressures.items():
        cur_press = ezca[channel]
        if not 69 < cur_press < 71:
            yield f'{loc} HEPI pressure out of range'


@SYSDIAG.register_test
def HW_INJ():
    """Hardware injections running
    Same as CW inj?
    """
    if ezca['CAL-INJ_STATUS'] != 3:
        yield 'Hardware injections stopped'


hws_gps_gen_chan = 'TCS-{}_HWS_LIVE_ACQUISITION_GPSTIME'
# Currently only CS HWS working --TJ
#hws_loc = ['ITMX', 'ITMY', 'ETMX', 'ETMY']
hws_loc = ['ITMX', 'ITMY']
hws_gps_chans = [hws_gps_gen_chan.format(loc) for loc in hws_loc]
hws_st = StaticTester(hws_gps_chans, static_time=60)
#@SYSDIAG.register_test
def HWS():
    msg_led = 'HWS SLED is OFF for {}'
    led_list = []
    msg_stopped = 'HWS code stopped for {}'
    stopped_list = []
    for loc in hws_loc:
        # 0 = OFF for now, dont believe epics. 2019-10-29
        if 'ETM' in loc:
            if ezca['TCS-{}_HWS_SLEDSHUTDOWN'.format(loc)] != 1:
                led_list.append(loc)
        else:
            if ezca['TCS-{}_HWS_SLEDSHUTDOWN'.format(loc)] != 0:
                led_list.append(loc)
        if hws_st.is_chan_static(hws_gps_gen_chan.format(loc)) and ezca['GRD-ISC_LOCK_STATE_N'] < 540:
            # From Dec 2023, HWS code should be stopped in NLN, alog74951
            stopped_list.append(loc)

    if led_list:
        yield msg_led.format(led_list)
    if stopped_list:
        yield msg_stopped.format(loc)


@SYSDIAG.register_test
def IMC_WFS():
    """Checks if IMC WFS need centering
    
    """
    locked_states = [50, 60, 70, 100]
    for ab in ['A','B']:
        if ezca['IMC-PWR_IN_OUT16'] > 0.2:
            for dof in ['PIT','YAW']:
                if ezca['GRD-IMC_LOCK_STATE_N'] in locked_states:
                    if abs(ezca['IMC-WFS_{}_DC_{}_OUT16'.format(ab, dof)]) > 0.75:
                        yield 'IMC WFS not centered'
                if ezca['GRD-IMC_LOCK_STATE_N'] == 80: # OFFLINE
                    if abs(ezca['IMC-WFS_{}_DC_{}_OUT16'.format(ab, dof)]) > 0.5:
                        yield 'IMC WFS not centered'
                    

@SYSDIAG.register
def IOP_DACKILL_MON():
    """Monitors IOP DACKILL status and reporting faults

    """
    for fec in iop_models:
        if int(ezca['FEC-{}_STATE_WORD'.format(fec.DCUID)]) & 1<<7:
            yield '{} ({}) IOP DACKILL Tripped'.format(fec.name, fec.DCUID)


@SYSDIAG.register_test
def LSC_OUTPUT():
    """Check that the LSC output signals are ON.

    """
    if ezca['LSC-CONTROL_ENABLE'] != 1:
        yield 'LSC output is not ON.'


@SYSDIAG.register_test
def MC_WFS():
    """MC WFS are engaged

    """
    if ezca['IMC-WFS_GAIN'] == 0:
        yield "not engaged"


#FIXME: The OMC PZT readbacks have been removed, we will need to find a new way
# to test the HV     ----TJ Jan 2 2018

def OMC_PZT_2():
    """More of just ideas so far. Trending shows that the last time we had the HV off,
    H1:OMC-PZT2_OUT was at 7208. I dont see it sitting at this point any other time.
    """
    pass

#@SYSDIAG.register_test
def OMC_PZT():
    """The OMC PZT2 high voltage should be on, otherwise the OMC_LOCK Guardian will fail
    at the FIND_CARRIER step
    """
    if ezca['OMC-PZT2_MON_DC_OUTMON'] < 1 and ezca['GRD-OMC_LOCK_STATE'] != 'OFF_RESONANCE':
        yield 'OMC PZT2 high voltage is OFF'


@SYSDIAG.register_test
def OPLEV_SUMS():
    """Optical lever sums nominal
    Check only when the suspension is aligned.
    """
    susOL = ['ITMX', 'ITMY', 'ETMX', 'ETMY']
    susOL2 = ['BS', 'PR3', 'SR3']
    tots = []
    for a in susOL:
        if ezca['GRD-SUS_' + a + '_STATE'] == 'ALIGNED':
            if ezca['SUS-' + a + '_L3_OPLEV_SUM_OUTMON'] < 500:
                tots.append(a)
    for b in susOL2:
        if ezca['GRD-SUS_' + b + '_STATE'] == 'ALIGNED':
            if ezca['SUS-' + b + '_M3_OPLEV_SUM_OUTMON'] < 500:
                tots.append(b)
    if tots:
        yield '{} sum(s) low'.format(tots)

# Check if PCAL has been over driven
gen_pcal_chan = 'CAL-PCAL{}_OFS_PD_OUT16'
pcal_chans = [gen_pcal_chan.format(xy) for xy in ['X', 'Y']]
pcal_static = StaticTester(pcal_chans, static_time=120, less_than=-7.5)

# Check if PCAL is otherwise saturate
gen_ofserr_pcal_chan = 'CAL-PCAL{}_OFS_ERR_OUT16'
pcalofs_chans = [gen_ofserr_pcal_chan.format(xy) for xy in ['X','Y']]
pcalofs_static = StaticTester(pcalofs_chans,static_time=120, less_than=-2.0)

# Temporarily removed TXPD test until we find new values and double check new channels
@SYSDIAG.register_test
def PCAL():
    """OFS servo malfunctions, toggling the loop seems to fix it."""
    for loc in ['X', 'Y']:
        if (pcal_static.is_chan_static(gen_pcal_chan.format(loc)) \
           or pcalofs_static.is_chan_static(gen_ofserr_pcal_chan.format(loc))):
            yield 'PCAL {} OFS servo malfunction'.format(loc)
    # TX
    #if ezca['GRD-ISC_LOCK_STATE_N'] >= 110: # DRMI
    #    rx_chan = 'CAL-PCAL{}_TX_PD_OUTPUT'.format(xy)
    #    rx_nom = rx_dict[xy]
    #    rx_avg = try_avg(-120, rx_chan)
    #    perc_diff = abs(rx_avg - rx_nom)/rx_nom
    #    if perc_diff > 0.01:
    #        yield '{} TX PD is greater than 1% off {}'.format(xy, rx_nom)


@SYSDIAG.register_test
def POP_PZT():
    """POPX PZT will rail from time to time. Heading to ISCT1 and making
    some adjustments may the way to fix it.
    """
    if ezca['ASC-POP_X_PZT_TRIG_MON'] == 1:
        if (abs(ezca['ASC-POP_X_PZT_PIT_OUTMON']) == ezca['ASC-POP_X_PZT_PIT_LIMIT']) or \
           (abs(ezca['ASC-POP_X_PZT_YAW_OUTMON']) == ezca['ASC-POP_X_PZT_YAW_LIMIT']):
            yield "POP X PZT railed, check it's centered and healthy enough"

@SYSDIAG.register_test
def PSL_AC():
    """Make sure that the Air Conditioning is not staying on in the Laser room.

    """
    Temp_thresh = 71
    if ezca['PSL-ENV_LASERRM_ACS_TEMP_DEGF'] < Temp_thresh and \
       ezca['PSL-ENV_LASERRM_ACN_TEMP_DEGF'] < Temp_thresh:
        yield 'PSL air conditioning is ON, or low temp in laser room'


@SYSDIAG.register_test
def PSL_CHILLER():
    """Alerts if the PSL chiller has some kind of alarm (usually low water level)
    """
    if ezca['PSL-LASER_CHILLER1_ENABLED'] != 1 or ezca['PSL-LASER_CHILLER1_FLOWERROR'] != 0 or ezca['PSL-LASER_CHILLER1_ALARM'] != 0:
        yield "Check PSL chiller"


psl_fss_dict = {'time': 0}
@SYSDIAG.register_test
def PSL_FSS():
    """PSL FSS not oscillating

    """
    cur_time = gpstime.gpsnow()
    if abs(ezca['PSL-FSS_FAST_MON_OUTPUT']) >= 7 and ezca['GRD-LASER_PWR_STATE'] != 'FAULT':
        if psl_fss_dict == 0:
            psl_fss_dict['time'] = cur_time
        elif cur_time - psl_fss_dict['time'] > 5:
            yield "PZT MON is high, may be oscillating"
    elif psl_fss_dict['time'] != 0:
        psl_fss_dict['time'] = 0


@SYSDIAG.register_test
def PSL_FSS_TPD():
    """Checks that the transmission through the reference cavity hasn't dropped too much.
    This was causing glitches and locklosses when powering up. If the TPD drops by 5dB
    you can increase FSS common gain by 5dB to compensate. Ideally the reference cavity
    alignment needs to be fixed though.

    https://alog.ligo-wa.caltech.edu/aLOG/index.php?callRep=65935
    """

    if ezca["PSL-FSS_TPD_DC_OUTPUT"] < 0.7:
        yield "RefCav transmission low"


@SYSDIAG.register_test
def PSL_ISS():
    """ISS diffracted power

    """
    diff_high = 5.5
    diff_low = 2.5
    if PSL_on():
        diff_now = ezca['PSL-ISS_DIFFRACTION_AVG']
        # if diff_now > diff_high:
        #     diff_pwr = try_avg(-10, 'PSL-ISS_DIFFRACTION_AVG')
        #     if diff_pwr >= diff_high:
        #         yield "Diffracted power is high"
        # elif diff_now < diff_low:
        #     diff_pwr = try_avg(-10, 'PSL-ISS_DIFFRACTION_AVG')
        #     if diff_pwr <= diff_high:
        #         yield "Diffracted power is low"
        if diff_now > diff_high:
            yield "Diffracted power high"
        elif diff_now < diff_low:
            yield "Diffracted power low"


@SYSDIAG.register_test
def PSL_NOISE_EATER():
    """PSL noise eater engaged

    """
    nominal = -5852

    value = ezca['PSL-MIS_NPRO_RRO_OUTPUT']
    if not (nominal - 50 <= value <= nominal + 50):
        yield "NPRO output out of range"


@SYSDIAG.register_test
def PSL_PMC_HV():
    """PMC high voltage is nominally around 4V, power glitches and other
    various issues can shut it off.
    """
    if -0.5 < ezca['PSL-PMC_HV_MON_AVG'] < 0.5:
        yield 'PMC high voltage is OFF'


@SYSDIAG.register_test
def QUAD_DAMPING():
    """An undamped quad will cause ISI ST2 to trip after some hours.
    There are two solutions for this:
        1. Damp the Quad
        2. Turn off the ST2 isolation
    """
    undamped = []
    for quad in ['ITMX', 'ITMY', 'ETMX', 'ETMY']:
        for dof in ['L', 'T', 'V', 'R', 'P', 'Y']:
            if ezca['SUS-{}_M0_DAMP_{}_STATE_OK'.format(quad, dof)] != 1:
                undamped.append((quad, 'M0'))
            if ezca['SUS-{}_R0_DAMP_{}_STATE_OK'.format(quad, dof)] != 1:
                undamped.append((quad, 'R0'))
    if undamped:
        for tup in undamped:
            if 'ISOLATED' in ezca['GRD-ISI_{}_ST2_STATE'.format(tup[0])]:
                yield '{} {} sus is not damped while ISI ST2 is isolated'.format(tup[0], tup[1])


@SYSDIAG.register_test
def RING_HEATERS():
    """Checks:
    * Top and bottom powers are similar
    * Only one half on
    * Completely off
    * Set power and readback power are not similar
    """
    # June 2022 TJ - As of right now we are using only the EX and EY RHs
    # Their values might change a bit, but it seems that we will use them
    # only check if they are off (below some value), let SDF keep actual power
    #optics = ['ITMX', 'ITMY', 'ETMX', 'ETMY']
    optics = ['ITMX', 'ETMY']
    off_list = []
    for opt in optics:
        upper = ezca['TCS-{}_RH_SETUPPERPOWER'.format(opt)]
        lower = ezca['TCS-{}_RH_SETLOWERPOWER'.format(opt)]
        # Check its on
        if lower == 0 and upper == 0:
            off_list.append(opt)
        # Check that not only one half is on
        elif (upper == 0 and lower != 0) or (upper != 0 and lower == 0):
            if upper == 0:
                yield '{} top is OFF, bottom ON'.format(opt)
            if lower == 0:
                yield '{} bottom is OFF, top is ON'.format(opt)
        # Percent difference of the top half and the bottom half power
        elif percent_diff(upper, lower) >= 30:
            yield '{} top and bottom powers are not similar'.format(opt)
    if off_list:
        yield '{} Ring heater(s) OFF'.format(off_list)

    # Power and readback not similar
    dead = []
    for optic in optics:
        for upper_lower in ['UPPER', 'LOWER']:
            power = ezca['TCS-{}_RH_{}POWER'.format(optic, upper_lower)]
            setpower = ezca['TCS-{}_RH_SET{}POWER'.format(optic, upper_lower)]
            # Low/off power
            if setpower <= 0.5:
                if abs(setpower - power) > 0.05:
                    dead.append((optic, upper_lower))
            # High power
            elif setpower > 0.5:
                if abs(setpower - power) > 0.1 * setpower:
                    dead.append((optic, upper_lower))
    if dead:
        yield '{} Ring Heater(s) maybe dead'.format(dead)


@SYSDIAG.register
def SEI_CPS_NOISEMON():
    """SEI CPS noise monitors
    Code from LLOalog69495, slightly modified
    """
    hamisis = ['HAM2','HAM3','HAM4','HAM5','HAM6','HAM7','HAM8']
    bscisis = ['ITMX','ITMY','ETMX','ETMY','BS']
    stages   = ['ST1','ST2']
    corners  = ['H1','H2','H3','V1','V2','V3']
    blrms   = ['65_100','130_200']
    blrms_thresh = 30
    noisy_hams = []
    noisy_bscs = []

    for corner in corners:
        for ham in hamisis:
            chan1 = 'ISI-' + ham + '_CPSINF_' + corner + '_BLRMS_65_100'
            chan2 = 'ISI-' + ham + '_CPSINF_' + corner + '_BLRMS_130_200'
            blrms1 = ezca[chan1]
            blrms2 = ezca[chan2]
            if blrms1 >= blrms_thresh or blrms2 >= blrms_thresh:
                noisy_hams.append((ham, corner))

    for corner in corners:
        for bsc in bscisis:
            for stage in stages:
                chan1 = 'ISI-' + bsc + '_' + stage + '_CPSINF_' + corner + '_BLRMS_65_100'
                chan2 = 'ISI-' + bsc + '_' + stage + '_CPSINF_' + corner + '_BLRMS_130_200'
                blrms1 = ezca[chan1]
                blrms2 = ezca[chan2]
                if blrms1 >= blrms_thresh or blrms2 >= blrms_thresh:
                    noisy_bscs.append((bsc, stage, corner))
    
    if noisy_hams:
        yield f'Noisy HAM CPS(s): {noisy_hams}'
    if noisy_bscs:
        yield f'Noisy BSC CPS(s): {noisy_bscs}'


@SYSDIAG.register_test
def SEI_IOP_WD():
    """SEI watchdog not tripped

    """
    hams = ['HAM2', 'HAM3', 'HAM4', 'HAM5', 'HAM6']
    chambers = ['BS', 'ITMX', 'ITMY', 'ETMX', 'ETMY']
    seis = hams + chambers
    tots = []
    for chamber in seis:
        if not ezca['IOP-SEI_' + chamber + '_DACKILL_STATE'] == 1:
            tots.append(chamber)
    if tots:
        yield "SEI IOP {} tripped".format(tots)


@SYSDIAG.register_test
def SEI_COILMON_ALL_OK():
    """ISI coilmon status

    """
    hams = ['HAM4', 'HAM5', 'HAM6']
    chambers = ['BS', 'ITMX', 'ITMY', 'ETMX', 'ETMY']
    bscs = chambers
    tots = []
    for chamber in bscs:
        if ezca['ISI-' + chamber + '_COILMON_STATUS_ALL_OK '] != 1:
            tots.append(chamber)
    for chamber in hams:
        if ezca['ISI-' + chamber + '_BIO_IN_COILMON_STATUS_ALL_OK'] != 1:
            tots.append(chamber)
    if tots:
        yield "ISI {} coilmon drop out".format(tots)


sei_conf_static = StaticTester(['GRD-SEI_CONF_STATUS'], static_time=150, val=2)
@SYSDIAG.register_test
def SEI_STATE():
    """SEI systems nominal

    Also checks if the WD saturation counts are are greater than some
    percent of the limit.

    """
    chambers = ['HAM2', 'HAM3', 'HAM4', 'HAM5', 'HAM6',
                'BS', 'ITMX', 'ITMY', 'ETMX', 'ETMY']
    sei = {
        'ham': ['HAM2', 'HAM3', 'HAM4', 'HAM5', 'HAM6'],
        'bsc_st1': ['ITMX', 'ITMY', 'ETMX', 'ETMY'],
        'bsc_st2': ['ITMX', 'ITMY', 'ETMX', 'ETMY'],
    }
    acts = {
        'ham': ['CPS', 'GS13', 'L4C', 'ACT'],
        'bsc_st1': ['CPS', 'T240', 'L4C', 'ACT'],
        'bsc_st2': ['CPS', 'GS13', 'ACT'],
    }
    count_percent = 0.75

    # nominal state
    not_nominal = []
    for chamber in chambers:
        if chamber == 'BS':
            if ezca['GRD-ISC_LOCK_STATE_N'] > 104 and ezca['GRD-SEI_BS_STATE'] != 'FULLY_ISOLATED':
                yield 'BS not in correct state'
        elif ezca['GRD-SEI_{}_OK'.format(chamber)] != 1:
            not_nominal.append(chamber)
    if not_nominal:
        if len(not_nominal) > 1:
            yield '{} are not nominal'.format(not_nominal)
        else:
            yield '{} is not nominal'.format(not_nominal)

    # Saturation counts
    tots_ham = []
    tots_st1 = []
    tots_st2 = []
    for chamber_type, chambers in sei.items():
        if chamber_type == 'ham':
            for chamber in chambers:
                for act in acts[chamber_type]:
                    if (ezca['ISI-{}_WD_{}_SAT_COUNT'.format(chamber, act)]
                        > count_percent * ezca['ISI-{}_WD_{}_SAFECOUNT'.format(chamber, act)]):
                        tots_ham.append(chamber)
        elif chamber_type == 'bsc_st1':
            for chamber in chambers:
                for act in acts[chamber_type]:
                    if (ezca['ISI-{}_ST1_WD_{}_SAT_COUNT'.format(chamber, act)]
                        > count_percent * ezca['ISI-{}_ST1_WD_{}_SAFECOUNT'.format(chamber, act)]):
                        tots_st1.append(chamber)
        elif chamber_type == 'bsc_st2':
            for chamber in chambers:
                for act in acts[chamber_type]:
                    if (ezca['ISI-{}_ST2_WD_{}_SAT_COUNT'.format(chamber, act)]
                        > count_percent * ezca['ISI-{}_ST2_WD_{}_SAFECOUNT'.format(chamber, act)]):
                        tots_st2.append(chamber)
        if tots_ham:
            yield 'ISI {} WD saturation count greater than 75% of max'.format(tots_ham)
        if tots_st1:
            yield 'ISI {} ST1 WD saturation count > 75% of max'.format(tots_st1)
        if tots_st2:
            yield 'ISI {} ST2 WD saturation count > 75% of max'.format(tots_st2)

    # Notify if the SEI_CONF is still not there
    #if ezca['GRD-SEI_CONF_STATUS'] != 3:
    #    yield 'SEI_CONF state is transitioning or stuck'
    if sei_conf_static.is_chan_static('GRD-SEI_CONF_STATUS'):
        yield 'SEI_CONF might be stuck'


@SYSDIAG.register_test
def SEISMON_EQ():
    """
       Check SEISMON is alive.
    """
    suffix = ['COMEXCEN', 'FASTEXCEN']
    prefix = ['IMC-REFL', 'LSC-REFL', 'ALS-X_REFL', 'ALS-Y_REFL', 'ALS-X_FIBR', 'ALS-Y_FIBR']
    for suf in suffix:
        for pre in prefix:
            if ezca['{}_SERVO_{}'.format(pre, suf)] != 0:
                yield '{}_SERVO_{} is ON'.format(pre, suf)

    AB = ['A', 'B']
    for ab in AB:
        if ezca['LSC-REFL_SUM_{}_EXCEN'.format(ab)] != 0:
            yield 'LSC-REFL_SUM_{}_EXCEN is ON'.format(ab)

    # If these voltages get too high, then we will lose lock
    # DIAG_MAIN will have this as a warning, then Verbal will give
    # an immediate threat notification.
    # Be careful here of excessive warning.
    volt_thresh = 8
    for chan in ['LSC-REFL_SERVO_SPLITMON', 'IMC-REFL_SERVO_SPLITMON']:
        if abs(ezca[chan]) > volt_thresh and ezca['GRD-ISC_LOCK_STATE_N'] > 11:
            yield '{} > {}V'.format(chan, volt_thresh)


@SYSDIAG.register_test
def SHUTTERS():
    """Monitor the Beckhoff controlled shutters

    """
    letters = {
        'C': ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'],
        'X': ['A', 'B'],
        'Y': ['A', 'B']
    }
    # When light does not the shutter PDs, they will close. So when the PSL goes out,
    # then we get notified of many shutters that are now closed.
    if not PSL_on():
        pass
    else:
        for loc, shut in letters.items():
            for shutter in shut:
                chan = 'SYS-MOTION_{}_SHUTTER_{}_STATE'.format(loc, shutter)
                chan_val = ezca[chan]
                isc_lock_state_n = ezca['GRD-ISC_LOCK_STATE_N']
                # Changing configuration
                if chan == 'SYS-MOTION_X_SHUTTER_A_STATE' or chan == 'SYS-MOTION_Y_SHUTTER_B_STATE':
                    if isc_lock_state_n > 307 and chan_val != 1:
                        yield 'ALS {} shutter not closed'.format(loc)
                elif chan == 'SYS-MOTION_C_SHUTTER_G_STATE':
                    # The shutters are wired to PDs, if the voltage on these PDs goes outside of
                    # some threshold, then the shutter will close. This particular shutter will
                    # close on lockloss and during DRMI in the follwing states.
                    if chan_val != 0 and (ezca['GRD-ISC_LOCK_STATE'] not in ['DOWN', 'LOCK_DRMI_1F']):
                        yield 'AS beam shutter open (nominal: closed)'
                elif chan == 'SYS-MOTION_X_SHUTTER_B_STATE':
                    if chan_val != 1:
                        yield 'ISCTEX fiber beam shutter is open (nominal: closed)'
                # Constant configuration
                elif chan in ['SYS-MOTION_Y_SHUTTER_A_STATE',
                              'SYS-MOTION_X_SHUTTER_B_STATE',
                              'SYS-MOTION_C_SHUTTER_H_STATE',
                              'SYS-MOTION_C_SHUTTER_B_STATE']:
                    if chan_val != 1:
                        yield '{} shutter open (nominal: closed)'\
                            .format(ezca['SYS-MOTION_{}_SHUTTER_{}_NAME'.format(loc, shutter)])
                elif chan_val != 0:
                    yield 'Shutter {} closed (nominal: open)'.format(shutter)


sqz_chan = 'SQZ-OPO_REFL_DC_POWER'
sqz_st = StaticTester([sqz_chan], static_time=60, greater_than=1.15)
#@SYSDIAG.register_test
def SQZ():
    """This test looks at the OPO REFL DC power and when there
    is a rise, we will see the range dip slowly.
    """
    if sqz_st.is_chan_static(sqz_chan) and ezca['GRD-ISC_LOCK_STATE_N'] >= 600:
        yield 'SQZ OPO REFL DC Power has increased.'


@SYSDIAG.register_test
def SQUEEZING():
    """This test checks that we are at the nominal SQZ_MANAGER state
    when we are at ISC_LOCK state INJECT_SQUEEZING or above.
    """
    if ezca['GRD-ISC_LOCK_STATE_N'] > 580:
        if int(ezca['GRD-SQZ_MANAGER_STATE_N']) != int(ezca['GRD-SQZ_MANAGER_NOMINAL_N']):
            yield 'SQZ_MANAGER is not in the nominal state. Check we are Squeezing.'
        if ezca['SQZ-OPO_ISS_OUTPUTRAMP'] == 1:
            yield 'ISS Pump is off. See alog 70050.'
    


#@SYSDIAG.register_test
def SR3_HEATER():
    """The SR3 heater should always be on and set at 5W
    notify if temp is out of range.

    If nominal operation moves from 5 W, the temp value will
    need to be updated.
    """
    cur_temp = ezca['AWC-SR3_HEATER_ELEMENT_TEMPERATURE']
    # 88 is around nominal, but this is a good wiggle for restarts, etc.
    if cur_temp < 70:
        yield 'SR3 heater is not heating'
    elif cur_temp > 100:
        yield 'SR3 heater is too hot! Ouch!'



@SYSDIAG.register_test
def SUS_RMS_WD():
    """RMS watchdogs

    """
    quads = ['ETMX', 'ETMY', 'ITMX', 'ITMY']
    for j in quads:
        tripped = []
        if int(ezca['SUS-' + j + '_BIO_L2_MON']) & 2 != 2:
            #yield "{} RMS UL WD has tripped".format(j)
            tripped.append('UL')
        if int(ezca['SUS-' + j + '_BIO_L2_MON']) & 32 != 32:
            tripped.append('LL')
        if int(ezca['SUS-' + j + '_BIO_L2_MON']) & 512 != 512:
            tripped.append('UR')
        if int(ezca['SUS-' + j + '_BIO_L2_MON']) & 8192 != 8192:
            tripped.append('LR')
        # Combine all of the quadrants
        if tripped:
            yield "{} RMS {} WD has tripped".format(j, tripped)


@SYSDIAG.register_test
def SUS_WD():
    """SUS watchdogs and OSEM input filter inmon on the quads

    """
    gen_osems = ['UL', 'LL', 'UR', 'LR']
    top_stage = ['F1', 'F2', 'F3', 'LF', 'RT', 'SD']
    quads = {
        'ITMX': {'M0': top_stage, 'L1': gen_osems, 'L2': gen_osems},
        'ITMY': {'M0': top_stage, 'L1': gen_osems, 'L2': gen_osems},
        'ETMX': {'M0': top_stage, 'L1': gen_osems, 'L2': gen_osems},
        'ETMY': {'M0': top_stage, 'L1': gen_osems, 'L2': gen_osems},
    }
    tripped = []
    for sus, level in suspensions.items():
        tripped_levels = []
        for lev in level:
            if not ezca['SUS-{}_{}_WDMON_STATE'.format(sus, lev)] == 1:
                tripped_levels.append(lev)
        if tripped_levels:
            tripped.append('{}({})'.format(sus, ','.join(tripped_levels)))
    if tripped:
        yield '{} WD Tripped'.format(', '.join(tripped))


@SYSDIAG.register_test
def SUS():
    """Check the suspensions are in a correct state.
    Main check is alignment for the non-managed suss.
    """
    # Check the sus that are NOT managed are aligned. Don't want to get caught trying
    # to lock with a sus misaligned.
    managed = ['ETMX', 'ETMY', 'ITMX', 'ITMY', 'OMC', 'SRM', 'SR2', 'MC2', 'PRM', 'PR2']
    aligned_sus = [x for x in suspensions if x not in managed]
    aligned = []
    for asus in aligned_sus:
        if ezca['GRD-SUS_{}_STATE'.format(asus)] != 'ALIGNED':
            aligned.append(asus)
    if aligned:
        yield '{} not aligned'.format(', '.join(aligned))


@SYSDIAG.register
def SUS_OSEM_CHECK():
    """SUS OSEM faulty read-back channel monitor.
    Contact Rahul if any issue arises.
    """

    # All SUS types and stages
    opos = ['OPO']
    hsss = ['RM1', 'RM2', 'OM1', 'OM3', 'IM1', 'IM2', 'IM3', 'IM4']
    hxds = ['ZM1', 'ZM2', 'ZM3', 'ZM4', 'ZM5', 'ZM6', 'OM2']
    tmts = ['TMSX', 'TMSY']
    omcs = ['OMC']
    ofis = ['OFI']
    bsfm = ['BS']
    hsts = ['MC1', 'MC2', 'MC3', 'PR2', 'PRM', 'SR2', 'SRM', 'FC1', 'FC2']
    hlts = ['PR3', 'SR3']
    quad = ['ITMX', 'ITMY', 'ETMX', 'ETMY']

    # All OSEM configurations
    osemsF = ['F1', 'F2', 'F3', 'LF', 'RT', 'SD']
    osemsT = ['T1', 'T2', 'T3', 'LF', 'RT', 'SD']
    osemsH = ['H1', 'H2', 'H3', 'V1', 'V2', 'V3']
    osems3 = ['LF', 'RT', 'SD']
    osems4 = ['UL', 'LL', 'UR', 'LR']

    # Define faulty OSEM ADC count threshold
    ADClimit = 200

    # Since this could have false alarms during large lock losses,
    # collect all of the notifications and put them together.
    broken_list = []

    for j in hsss:
        for k in osems4:
            if abs(int(ezca['SUS-'+j+'_M1_OSEMINF_'+k+'_INMON'])) <= ADClimit:
                broken_list.append((j, 'M1', k))

    for j in hxds:
        for k in osems4:
            if abs(int(ezca['SUS-'+j+'_M1_OSEMINF_'+k+'_INMON'])) <= ADClimit:
                broken_list.append((j, 'M1', k))

    for j in tmts:
        for k in osemsF:
            if abs(int(ezca['SUS-'+j+'_M1_OSEMINF_'+k+'_INMON'])) <= ADClimit:
                broken_list.append((j, 'M1', k))

    for j in omcs:
        for k in osemsT:
            if abs(int(ezca['SUS-'+j+'_M1_OSEMINF_'+k+'_INMON'])) <= ADClimit:
                broken_list.append((j, 'M1', k))

    for j in opos:
        for k in osemsH:
            if abs(int(ezca['SUS-'+j+'_M1_OSEMINF_'+k+'_INMON'])) <= ADClimit:
                broken_list.append((j, 'M1', k))

    for j in ofis:
        for k in osems3:
            if abs(int(ezca['SUS-'+j+'_M1_OSEMINF_'+k+'_INMON'])) <= ADClimit:
                broken_list.append((j, 'M1', k))

    for j in bsfm:
        for k in osemsF:
            if abs(int(ezca['SUS-'+j+'_M1_OSEMINF_'+k+'_INMON'])) <= ADClimit:
                broken_list.append((j, 'M1', k))

    for j in bsfm:
        for k in osems4:
            if abs(int(ezca['SUS-'+j+'_M2_OSEMINF_'+k+'_INMON'])) <= ADClimit:
                broken_list.append((j, 'M1', k))

    for j in hsts + hlts:
        for k in osemsT:
            if abs(int(ezca['SUS-'+j+'_M1_OSEMINF_'+k+'_INMON'])) <= ADClimit:
                broken_list.append((j, 'M1', k))

    for j in hsts + hlts:
        for k in ['M2','M3']:
            for l in osems4:
                if abs(int(ezca['SUS-'+j+'_'+k+'_OSEMINF_'+l+'_INMON'])) <= ADClimit:
                    broken_list.append((j, k, l))

    for j in quad:
        for k in ['M0','R0']:
            for l in osemsF:
                if abs(int(ezca['SUS-'+j+'_'+k+'_OSEMINF_'+l+'_INMON'])) <= ADClimit:
                    broken_list.append((j, k, l))

    for j in quad:
        for k in ['L1','L2']:
            for l in osems4:
                if abs(int(ezca['SUS-'+j+'_'+k+'_OSEMINF_'+l+'_INMON'])) <= ADClimit:
                    broken_list.append((j,k,l))

    if broken_list:
        # If the total is longer than the 55 character limit for the
        # message field, split it up. 3 gets us close to 55
        if len(broken_list) > 3:
            grouped = [broken_list[n:n+3] for n in range(0, len(broken_list), 3)]
            for group in grouped:
                yield f'OSEMs in fault: {group}'
        else:
            yield f'OSEMs in fault: {broken_list}'


@SYSDIAG.register_test
def TCS_LASER():
    """TCS laser OK

    Switched ON
    Power above 50W
    No Flow Alarm
    No IR alarm
    """
    for arm in ['X', 'Y']:
        laser_switch = ezca['TCS-ITM{}_CO2_LASERONOFFSWITCH'.format(arm)]
        if not laser_switch == 1:
            yield "{} laser switched OFF".format(arm)
        # flow alarm
        elif ezca['TCS-ITM{}_CO2_INTRLK_FLOW_ALRM'.format(arm)] != 0:
            yield "{} flow alarm".format(arm)
        # IR alarm
        elif ezca['TCS-ITM{}_CO2_INTRLK_RTD_OR_IR_ALRM'.format(arm)] != 0:
            yield "{} IR alarm".format(arm)
        # if the power gets below 50 there is a problem
        elif laser_switch == 1 and not (ezca['TCS-ITM{}_CO2_LSRPWR_HD_PD_OUTPUT'.format(arm)] > 20):
            yield "{} power too low".format(arm)
   
            
@SYSDIAG.register_test
def TCS_POWER():
    """This test checks if the CO2 X/Y masks and power are
     in nominal state when the IFO is in nominal low noise.
    """
    if ezca['GRD-ISC_LOCK_STATE_N'] >= 600:
        CO2XPower = ezca['TCS-ITMX_CO2_LSRPWR_MTR_OUT16']
        CO2YPower = ezca['TCS-ITMY_CO2_LSRPWR_MTR_OUT16']
        
        if (ezca['GRD-TCS_CO2X_MASKS_STATE_N'] != 40 or ezca['GRD-TCS_CO2Y_MASKS_STATE_N'] != 40):
            yield 'C02 Masks are not in nominal state.'
        if not 1.6 <= CO2XPower <= 1.85 or not 1.6 <= CO2YPower <= 1.85 :
            yield 'C02 Powers are not in nominal state.'
           

#@SYSDIAG.register_test
def TEST():
    """Used to test different things that need testing,
    by running a test labeled TEST.
    This is a test.
    """
    av = try_avg(-7, 'PSL-ISS_DIFFRACTION_AVG')
    if av > 3:
        yield 'too high'


@SYSDIAG.register_test
def TIDAL_LIMITS():
    """Tidal limits within range

    Check if channel reaches to 10% of the limit value.

    """
    # LSC Tidal
    arms = ['X', 'Y']
    place = ['ARM', 'TIDAL', 'COMM']
    for axis in arms:
        for j in place:
            if percent_diff(ezca['LSC-%s_%s_CTRL_LIMIT' % (axis, j)],
                            ezca['LSC-%s_%s_CTRL_OUT16' % (axis, j)]) <= 10:
                yield '%s_%s_CTRL ouput within 10 percent of limit' % (axis, j)

    # HEPI tidal limits
    # This limit was chosen to give an hour or two of notice before the limit is reached.
    limit_perc = 0.95
    dofs = ['X', 'Y']
    for dof in dofs:
        # This line is to avoid a zero division error
        if ezca['HPI-ETM{}_ISCINF_LONG_LIMIT'.format(dof)]:
            if (ezca['HPI-ETM{}_ISCMON_{}_INMON'.format(dof, dof)] \
                / ezca['HPI-ETM{}_ISCINF_LONG_LIMIT'.format(dof)]) \
                > limit_perc:
                yield 'HEPI ETM{} is within {}% of its limits'.format(dof, limit_perc * 100)


vio_chan = 'OAF-RANGE_RLP_{}_OUTPUT'
slope_threshold = 5e-5 # probably needs tuning
vio_blrms = [8, 9]
vio_timers = {'5min': 0,
              '30min': 0}
vio_levels = {blrm: [] for blrm in vio_blrms}

#@SYSDIAG.register_test
def VIOLINS():
    """Checks for violin mode ringups
    
    """

    def get_vio_values():
        # Gets max value of violin BLRMS over past 5 minutes
        for blrm in vio_blrms:
            data = getdata([vio_chan.format(blrm)], -300)
            if data:
                value = data[0].data.max()
                vio_levels[blrm].append(value)
            else:
                log('Failed to get data')
        
    def vio_slopes_okay():
        for blrm in vio_levels:
            slope = (vio_levels[blrm][-1] - vio_levels[blrm][0]) / 1800
            if slope > slope_threshold:
                return False
    
    if ezca['GRD-VIOLIN_DAMPING_STATE_N'] == 37 and ezca['GRD-ISC_LOCK_STATE_N'] >= 600: # DAMP_VIOLINS_FULL_POWER, NLN
        time_now = int(gpstime.tconvert())
        if vio_timers['5min'] == 0 and vio_timers['30min'] == 0:
            get_vio_values()
            for ent in vio_timers:
                vio_timers[ent] = time_now
        if time_now - vio_timers['5min'] > 300:
            get_vio_values()
            vio_timers['5min'] = time_now
            if time_now - vio_timers['30min'] > 1800:
                if not vio_slopes_okay():
                    yield 'Violin modes increasing over 30 minutes!'
                vio_timers['30min'] = time_now
            for blrm in vio_levels:
                if len(vio_levels[blrm]) > 7:
                    del vio_levels[blrm][0]
    else:
        for blrm in vio_levels:
            vio_levels[blrm] = []
        for ent in vio_timers:
            vio_timers[ent] = 0


@SYSDIAG.register_test
def WHITENING():
    """Whitening and Anti-whitening filters both set equally

    """
    prefix = [
        'ASC-REFL_A_RF9',
        'ASC-REFL_A_RF45',
        'ASC-REFL_B_RF9',
        'ASC-REFL_B_RF45',
        'ASC-POP_X_RF',
        'ASC-AS_A_RF45',
        'ASC-AS_A_RF36',
        'ASC-AS_A_RF72',
        'ASC-AS_B_RF45',
        'ASC-AS_B_RF36',
        'ASC-AS_B_RF72',
        'ASC-POP_A',
        'ASC-POP_B',
        'ASC-OMC_A',
        'ASC-OMC_B',
        'ASC-OMCR_A',
        'ASC-OMCR_B',
        'ASC-AS_C',
        'IMC-WFS_A',
        'IMC-WFS_B',
        'IMC-MC2_TRANS',
        'IMC-IM4_TRANS',
        'LSC-REFLAIR_A_RF9',
        'LSC-REFLAIR_A_RF45',
        'LSC-REFLAIR_B_RF27',
        'LSC-REFLAIR_B_RF135',
        'LSC-POPAIR_A_RF9',
        'LSC-POPAIR_A_RF45',
        'LSC-POPAIR_B_RF18',
        'LSC-POPAIR_B_RF90',
        # These are no more, unless they come back...
        #'LSC-ASAIR_A_RF45',
        #'LSC-ASAIR_B_RF18',
        #'LSC-ASAIR_B_RF90',
        'LSC-REFL_A_RF9',
        'LSC-REFL_A_RF45',
        'LSC-POP_A_RF9',
        'LSC-POP_A_RF45',
        #'PSL-ISS_SECONDLOOP_QPD',
        'ALS-X_QPD_A',
        'ALS-X_QPD_B',
        'ALS-X_WFS_A',
        'ALS-X_WFS_B',
        'ASC-X_TR_A',
        'ASC-X_TR_B',
        'ALS-Y_QPD_A',
        'ALS-Y_QPD_B',
        'ALS-Y_WFS_A',
        'ALS-Y_WFS_B',
        'ASC-Y_TR_A',
        'ASC-Y_TR_B',
    ]

    # DB LHO 11july2017: h1asc model changed RF90 to RF72, Beckhoff retained RF90 names. Make exception for AS_[A,B]_RF90/72 mapping
    #rf72_tups = [('ASC-AS_{}_RF90_WHITEN_FILTER_{}'.format(ab,i), 'ASC-AS_{}_RF72_AWHITEN_SET{}'.format(ab,i)) for ab in ['A', 'B'] for i in range(1,4)]
    #omc_tups = [('OMC-DCPD_A_WHITEN_SET_{}'.format(i),
                 #'OMC-DCPD_B_WHITEN_SET_{}'.format(i)) for i in range(1, 4)]
    all_dem_tups = [('{}_WHITEN_FILTER_{}'.format(pre, i),
                     '{}_AWHITEN_SET{}'.format(pre, i))
                    for pre in prefix for i in range(1, 4)] #+ omc_tups

    for x in all_dem_tups:
        if ezca[x[0]] != ezca[x[1]]:
            yield '{} and Anti-whiten not equal'.format(x[0])
