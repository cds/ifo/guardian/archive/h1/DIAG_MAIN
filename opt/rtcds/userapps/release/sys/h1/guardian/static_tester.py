import gpstime



class StaticTester():
     """Designed to be used in DIAG_MAIN. Check if a channel has stayed
     static for too long. Will only take one point every second.

     chans         -  List of chans to test.
     static_time   -  Integer time in seconds to chek static.
     val           -  If a value is given, it will only start timing the channel
                      if the channel is the given value. Default is None which
                      will time every static value.
     greater_than  -  If a value is given, it wont start timing until
                      the channel goes above the value.
     less_than     -  If a value is given, it wont start timing until
                      the channel goes below the value.


     Example:

     >>> sei_conf_static = StaticTester(['GRD-SEI_CONF_STATUS'], static_time=150, val=2)
     >>> sei_conf_static.is_chan_static('GRD-SEI_CONF_STATUS')
     False

     """

     time_now = int(gpstime.tconvert())
     cur_chan = None

     def __init__(self, chans, static_time=5, val=None, greater_than=None, less_than=None):
         self.master_list = {chan: [(0, 0)] for chan in chans}
         self.static_time = static_time
         self.val = val
         self.gt = greater_than
         self.lt = less_than

     @property
     def gps_all(self):
         return [tup[1] for tup in self.master_list[self.cur_chan]]

     @property
     def vals_all(self):
         return [tup[0] for tup in self.master_list[self.cur_chan]]

     def make_point(self):
         self.master_list[self.cur_chan].append((ezca[self.cur_chan], self.time_now))

     def remove_point(self):
         del self.master_list[self.cur_chan][0]

     def is_chan_static(self, chan):
         """Main method called. Should use other needed methods.
         Will return True if static
         """
         # Get currents
         self.cur_chan = chan
         self.time_now = int(gpstime.tconvert())

         # Check if list current, add point if not
         if self.gps_all[-1] < self.time_now:
             self.make_point()
         # If just started, get more data
         if len(self.gps_all) < self.static_time:
             return False
         # Make sure list isnt too long
         elif len(self.gps_all) > self.static_time + 1:
             self.remove_point()
         # Lets now do the check
         if self.gt != None:
             if all(i > self.gt for i in self.vals_all):
                 return True
             else:
                 return False
         if self.lt != None:
             if all(i < self.lt for i in self.vals_all):
                 return True
             else:
                 return False
         # If all vals are the same and time is good
         unique = set(self.vals_all)
         if len(unique) == 1 \
            and max(self.gps_all) - min(self.gps_all) >= self.static_time:
             if self.val != None and unique != {self.val}:
                 return False
             else:
                 return True
         else:
             return False
